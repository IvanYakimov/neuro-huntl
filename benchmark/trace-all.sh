#!/bin/bash

if [[ ! -d traces ]]; then
    mkdir traces
fi

for f in sources/*.c; do
    ./tracer.sh $f
done

mv *.trace traces
touch all-traced
