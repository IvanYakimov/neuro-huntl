#!/bin/bash

function win2deb {
    target=$1
    echo $target
    cat $target | tr -d '\32' > $target.tmp
    mv $target.tmp $target
}

cd openglbk
src_list=$(ls *)
for src in $src_list; do
    if [[ -f $src ]]; then
	win2deb $src
    fi
done

