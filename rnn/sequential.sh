#!/bin/bash
if [[ $# != 2 ]]; then
    echo 'Error - wrong number of arguments!'>&2 && exit 1; fi
epochs=$1
dataset=$2
log=log.txt
python2.7 sequential.py --batch 32 --epochs $epochs --ifile $dataset --ofile $(basename ${dataset} .dataset)_${epochs}_${log}

