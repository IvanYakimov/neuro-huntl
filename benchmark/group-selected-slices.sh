#!/bin/bash

TARGET=
DEST=

# parse command line arguments
OPTIND=1         # Reset in case getopts has been used previously in the shell.
# Initialize our own variables:
while getopts "h?vi:o:" opt; do
    case "$opt" in
	h|\?) echo 'Please, read the scipt' && exit 0;;
	v)  VERBOSE=true;;
	i)  TARGET=$OPTARG;;
	o)  DEST=$OPTARG;;
    esac
done
shift $((OPTIND-1))
[ "$1" = "--" ] && shift

# Check arguments
if [[ -z $TARGET || -z $DEST ]]; then
    echo "Error: please, set source and target files">&2 && exit 1; fi

# Group slices produced from the same source file
evaluate_slices() {
    extract_name() {
	name=$(basename $1 .slice)
	# print name without the last 2 characters are id of the slice
	echo ${name::-2}
    }

    # simply prints pairs <slice> <size of slice>
    print_out() {
	print_summary() {
	    echo ">${curname}"
	}	
	curname=''
	while read file; do
	    # get basic name and size of the slice
	    name=$(extract_name $file)
	    size=$(cat $file | wc -l)
	    # on first step
	    if [[ -z $curname ]]; then
		curname=$name; fi	    
	    # update current name if name changed
	    if [[ $curname != $name ]]; then
		print_summary
		curname=$name; fi	    
	    echo "$(basename $file) $size"
	done
	print_summary
    }
    cat $TARGET | sort -k1 | print_out
}

# divide slices into the groups and save the result into the JSON file
evaluate_slices | ./group-selected-slices.py > $DEST
