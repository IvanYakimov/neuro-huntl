// Copyright 2017 Ivan Yakimov
// Licensed under the Apache License, Version 2.0
// e-mail: ivan.yakimov.research@yandex.ru
#ifndef __CALL_TRACER__
#define __CALL_TRACER__

#include "llvm/IR/InstVisitor.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/FileSystem.h"

#include "func-checker.hpp"

#include <iostream>
#include <exception>

namespace trans {
  class CallTracer : public llvm::InstVisitor<CallTracer> {
  public:
    void visitReturnInst(llvm::ReturnInst &ret);
    void visitCallInst(llvm::CallInst &call);
    int GetCallNumber();

    CallTracer(llvm::raw_fd_ostream &outfile);
    ~CallTracer();
    
  private:
    int call_number_ = 0;
    FuncCheckerPtr checker_ = nullptr;
    llvm::raw_fd_ostream &outfile_;
    //    const char* separator_ = "# END_OF_FUNCTION";
  };
};

#endif
