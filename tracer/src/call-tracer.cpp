#include "call-tracer.hpp"

using namespace llvm;

namespace trans{

  CallTracer::CallTracer(raw_fd_ostream &outfile)
    : outfile_(outfile) {
    checker_ = FuncChecker::GetInstance();
  }
  
  CallTracer::~CallTracer() {
  }

  void CallTracer::visitReturnInst(llvm::ReturnInst &ret) {
    //    errs() << "VISITING RETURN INSTRUCTION\n";
    outfile_ << "#\n";
  }
  
  void CallTracer::visitCallInst(llvm::CallInst &call) {
    // errs() << "VISITING CALL INSTRUCTION\n";
    auto callee = call.getCalledFunction();
    try {
      //      errs() << "auto name = callee->getName().str();\n";
      if (not callee) {
	errs() << "Error on getting callee\n"; return;
      }
      
      auto name = callee->getName().str();
      //      errs() << "auto status = checker_->Check(callee);\n";
      auto status = checker_->Check(callee);
      //      errs() << "if (status) {\n";
      if (status) {
	outfile_ << name << "\n";
      }
    }
    catch (...) {
      outs() << "Exception occured\n";
    }
  }
  
  int CallTracer::GetCallNumber() {
    return call_number_;
  }
};
