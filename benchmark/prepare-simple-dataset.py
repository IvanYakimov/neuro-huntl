#!/usr/bin/python2.7

import json
import argparse

parser = argparse.ArgumentParser(description='arrange groupped slices into data batches of equal size')
parser.add_argument("-i", "--items", help="file with items", type=str)
parser.add_argument("-l", "--labels", help="file with labels", type=str)
parser.add_argument("-s", "--slices", help="file with slices", type=str)
parser.add_argument("-d", "--dest", help="destination", type=str)
parser.add_argument("-f", "--folder", help="folder with slices", type=str)
parser.add_argument("-z", "--zipping", help="zipping", type=bool)
parser.add_argument("-m", "--maxlen", help="maximum zipping length", type=int)
parser.add_argument("-I", "--startIdx", help="starting index", type=int)
args = parser.parse_args()

startIdx = 0
slices=[]
data=[]
dataset={}

itemsFile = args.items
labelsFile = args.labels
sliceList = args.slices
dist = args.dest
folder = args.folder
zipping = args.zipping
maxlen = args.maxlen
startIdx = args.startIdx
    
def loadList(fname):
    s = []
    with open(fname) as f:
        for line in f:
            s.append(line.rstrip())
    return s

def initMap(fname):
    l = loadList(fname)
    res = dict(zip(l,range(len(l))))
    return res

def sliceToChunks(sname):
    calls = loadList(sname)
    xs = []
    ys = []
    # with deletion
    for i in range(len(calls)):
        x = calls[:]
        y = calls[i]
        if zipping:
            x = map(lambda i: items[i], x)
            x = [0] * (maxlen - len(x)) + x
            y = labels[y]
            assert len(x) == maxlen
        else:
            assert len(x) == len(calls)-1
        xs.append(x)
        ys.append(y)
    # without deletion
    x = calls[:]
    x = map(lambda i: items[i], x)
    x = [0] * (maxlen - len(x)) + x
    y = '__none__'
    y = labels[y]
    xs.append(x)
    ys.append(y)
    return xs, ys

items = initMap(itemsFile)
labels = initMap(labelsFile)

with open(sliceList) as infile:
    t = infile.read()
    slices = json.loads(t)

for groupIndex in range(len(slices)):
    sliceGroup = slices[groupIndex]
    xs = []
    ys = []
    for baseName in sliceGroup.keys():
        for sliceFile in sliceGroup[baseName]:
            curxs,curys = sliceToChunks(folder + sliceFile)
            xs += curxs
            ys += curys
    data.append({"xs":xs, "ys":ys})
    assert len(data[groupIndex]["xs"]) == len(data[groupIndex]["ys"])

dataset["data"] = data
dataset["items"] = items
dataset["labels"] = labels
print ("Len of data", str(len(data)))

with open(dist, 'w') as outfile:
    outfile.write(json.dumps(dataset))
