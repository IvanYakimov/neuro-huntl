#!/usr/bin/python2.7

import numpy as np

from keras.layers.core import Reshape, Flatten
from keras.models import Sequential
from keras.layers import Input, LSTM, GRU, Dense
from keras.layers import Activation
from keras.layers import Dropout
from keras.layers import Embedding
from keras.utils import to_categorical
from keras.layers import Conv1D, GlobalAveragePooling1D, MaxPooling1D

from json import loads

import argparse
import models as md

from functools import reduce

parser = argparse.ArgumentParser(description='arrange groupped slices into data batches of equal size')
#parser.add_argument("-l", "--maxlen", help="Maxlen of the sequence", type=int)
parser.add_argument("-b", "--batch", help="Batch size", type=int)
parser.add_argument("-e", "--epochs", help="Epochs number", type=int)
parser.add_argument("-i", "--ifile", help="Dataset", type=str)
parser.add_argument("-o", "--ofile", help="Log", type=str)

args = parser.parse_args()

#input_length=args.maxlen
batch=args.batch
epoch_num=args.epochs
infile=args.ifile
outfile=args.ofile

dataset=[]
with open(infile, 'r') as f:
    c = f.read()
    dataset=loads(c)
    
num_kinds=len(dataset["labels"])
learn_xs = []
learn_ys = []
for i in range (0,8):
    learn_xs += dataset["data"][i]["xs"]
    learn_ys += dataset["data"][i]["ys"]
learn_xs = np.asarray(learn_xs)
learn_ys = np.asarray(learn_ys)

input_length = len(learn_xs[0])

test_xs = np.asarray(dataset["data"][9]["xs"])
test_ys = np.asarray(dataset["data"][9]["ys"])                     
print len(learn_xs), len(learn_ys)
print len(test_xs), len(test_ys)

model = md.gru_with_embedding(num_kinds, input_length)

# Data convertion
one_hot_learn = to_categorical(learn_ys, num_classes=num_kinds)
one_hot_test = to_categorical(test_ys, num_classes=num_kinds)

# Training
model.fit(learn_xs, one_hot_learn, epochs=epoch_num, batch_size=batch)

# Testing
score = model.evaluate(test_xs, one_hot_test, batch_size=batch)
print "loss: {0}, acc: {1}".format(score[0], score[1])

def getFunctionNameById(pred_idx):
    global dataset
    assert pred_idx in dataset["labels"].values()
    for k in dataset["labels"]:
        if dataset["labels"][k] == pred_idx:
            return k

# Get prediction
predictions = model.predict(test_xs, batch_size=len(test_xs))

with open (outfile, 'w') as outfile:
    for iter_num, pred in enumerate(predictions):
        real_idx = test_ys[iter_num]
        real_funcname = getFunctionNameById(real_idx)
        funcnames = []
        strike = 0
        for k in range(1,11):
            idx = np.argmax(pred)
            func = getFunctionNameById(idx)
            if (idx == real_idx):
                strike = k
            funcnames.append(func)
            pred[idx] = 0
        # Print output in a rational way
        if strike != 0:
            line = reduce(lambda x,y: x + " " + y, funcnames)
            outfile.write("{0} {1} {2} \n".format(real_funcname, strike, line))
