#include "context-maker.hpp"

using namespace llvm;

namespace context {
ContextVisitor::ContextVisitor() {}
ContextVisitor::~ContextVisitor() {}

void ContextVisitor::visitCallInst(CallInst &call) {
  auto callee = call.getCalledFunction();
  if (not callee) {
    return;
  }
  auto name = callee->getName().str();
  if (name == "glutDisplayFunc" or
      name == "glutReshapeFunc") {
    errs() << "Callback registration function detected "
	   << name
	   << "\n";
    int numop = call.getNumOperands();
    auto op0 = call.getOperand(0);
    auto op1 = call.getOperand(1);
    errs() << "op0: " << *op0 << "\n";
    errs() << "op1: " << *op1 << "\n";
    errs() << numop << "\n";
    for (auto arg = callee->arg_begin();
	 arg != callee->arg_end(); arg++) {
      errs() << *arg;//->getName().str();
    }
    
    errs() << "\n";
    errs().flush();
  }
}
};
