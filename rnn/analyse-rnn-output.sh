if (( $# != 1 )); then
	echo "error: log name is expected"
	exit 1
fi	

logfile=$1
echo $1

# Use to get 1 predictoin
#cat $logfile | awk {'print $2'} | grep -v ____ | sort | uniq -c | awk {'print $2 " " $1'} | sort -k1 > log-report
# Use to get top 3 predictoins
cat $logfile | awk {'print $1'} | sort | uniq -c | awk {'print $2 " " $1'} | sort -k1 > log-report
cat log-report | awk {'print $1'} > matched
grep -wf matched ../bin/distribution.list | awk {'print $2 " " $1'} | sort -k1 > matched-dist
grep -wvf matched ../bin/distribution.list | awk {'print $2 " " $1'} | sort -k1 > unmatched-dist
echo "Function Popularity Predicted"
paste log-report matched-dist | awk {'print $1 " " $4 " " $2'}
cat unmatched-dist | awk {'print $1 " " $2 " " 0'}
