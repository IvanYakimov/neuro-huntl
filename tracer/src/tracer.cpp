#include "tracer.hpp"

using std::string;

// http://stackoverflow.com/questions/6417817/easy-way-to-remove-extension-from-a-filename
string BaseName(string name) {
  size_t lastindex = name.find_last_of("."); 
  string rawname = name.substr(0, lastindex);
  return rawname;
}

void Transform(llvm::Module &module) {
  auto name = module.getModuleIdentifier();
  std::string err;
  string filename = BaseName(name) + string(".trace");
  llvm::raw_fd_ostream outfile(filename.c_str(), err, llvm::sys::fs::OpenFlags::F_RW);
  trans::CallTracer tr(outfile);
  tr.visit(module);
  outfile.close();
}

void MakeContext(llvm::Module &module) {
  context::ContextVisitor cm;
  cm.visit(module);
}

bool TracerPass::runOnModule(llvm::Module &module) {
  // TODO: start analysing with main
  // MakeContext(module);
  Transform(module);

  // some transformations done
  return false;
}
