#!/bin/bash

################################################################################
# Returns first X% most popular GL functions called in the examples.
################################################################################

if [[ $# != 1 ]]; then
    echo "Error: wrong number of arguments. Percentage is expected." >&2; exit 1
fi

re='^[0-9]+$'
num=$1
if ! [[ $1 =~ $re ]] ; then
   echo "Error: $1 is not a number" >&2; exit 1
fi

if ! [[ -f distribution-gl.list ]]; then
    echo "Not a file: distribution-gl.list" >&2; exit 1
fi

total=$(cat distribution-gl.list | wc -l)
top=$(echo "${total} * ${num} / 100" | bc)
cat distribution-gl.list | awk {'print $1 " " $2'} | head -n $top

