#!/bin/bash

MAX=
MIN=
DIR=
TGT=
VERBOSE=false
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# parse args
while getopts "h?vf:d:m:M:" opt; do
    case "$opt" in
	h|\?) echo 'Try reading the script' && exit 0;;
	v)  VERBOSE=true;;
	f)  TGT=$OPTARG;;
	d)  DIR=$OPTARG;;
	m)  MIN=$OPTARG;;
	M)  MAX=$OPTARG;;
    esac
done
shift $((OPTIND-1))
[ "$1" = "--" ] && shift

# do noting
pass() {
    echo "">/dev/null
}

# print result in a 'smart' way
filter_out() {
    verbose=$1
    while read input; do
	if $verbose; then
	    echo $input | awk {'print $1 " " $2'}
	else
	    echo $input | awk {'print $1'}; fi
    done
    }

# process target directory, skip files of unappropriate size
process_dir() {
    for slice in $DIR/*.slice; do
	size=$(cat $slice | wc -l)
	if (( $size < $MIN )); then
	    pass
	elif (( $size > $MAX )); then
	    pass
	else
	    echo "$slice $size"; fi	    
    done
}

if [[ -z $DIR || -z $TGT ]]; then
    echo "Error: please, set source and target files">&2 && exit 1; fi

process_dir | filter_out $VERBOSE > $TGT
