#!/bin/bash

for f in traces/*.trace; do
    ./slicer.sh $f
done

mv *.slice slices/

touch all-sliced
