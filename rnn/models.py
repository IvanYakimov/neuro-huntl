from keras.layers.core import Reshape, Flatten
from keras.models import Sequential
from keras.layers import Input, LSTM, GRU, Dense, SimpleRNN
from keras.layers import Activation
from keras.layers import Dropout
from keras.layers import Embedding
from keras.utils import to_categorical
from keras.layers import Conv1D, GlobalAveragePooling1D, MaxPooling1D

def simple(num_kinds, input_length):
    dp = 0.1
    third_hidden=num_kinds
    second_hidden=int(third_hidden * 2)
    first_hidden=int(second_hidden * 2)
    model = Sequential()
    model.add(Dense(first_hidden, activation='sigmoid', input_dim=length))
    model.add(Dropout(dp))
    model.add(Dense(second_hidden, activation='sigmoid'))
    model.add(Dropout(dp))
    model.add(Dense(third_hidden, activation='sigmoid'))
    model.add(Dropout(dp))
    model.add(Dense(num_kinds, activation='softmax'))    
    model.summary()
    model.compile(optimizer='rmsprop',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    return model

def lstm(num_kinds, input_length):
    global learn_xs
    global test_xs
    learn_xs = np.reshape(learn_xs, (len(learn_xs), length, 1))
    test_xs = np.reshape(test_xs, (len(test_xs), length, 1))
    input_shape=learn_xs.shape
    print "Learn xs shape: ", input_shape
    model = Sequential()
    model.add(GRU(num_kinds, return_sequences=False, input_shape=(length, 1)))
    model.add(Dense(num_kinds, activation='softmax'))
    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    model.summary()
    return model

def gru_with_embedding(num_kinds, input_length):
    embd = 32
    dp = 0.1
    model = Sequential()
    model.add(Embedding(num_kinds, embd, input_length=input_length))
    model.add(LSTM(32, dropout=dp, recurrent_dropout=dp, use_bias=True, activation='tanh', recurrent_activation='hard_sigmoid'))
    #model.add(SimpleRNN(32, dropout=dp, recurrent_dropout=dp, use_bias=True, activation='tanh'))
    model.add(Dropout(dp))	
    model.add(Dense(num_kinds, activation='softmax'))
    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    model.summary()
    return model

def deep_embedding(num_kinds, input_length):
    embd = 32
    model = Sequential()
    model.add(Embedding(num_kinds, embd, input_length=length))
    model.add(Dropout(0.1))	
    model.add(GRU(64, dropout=0.1, recurrent_dropout=0.1, use_bias=True, return_sequences=True))
    model.add(GRU(32, dropout=0.1, recurrent_dropout=0.1, use_bias=True))
    model.add(Dense(num_kinds, activation='softmax'))
    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    model.summary()
    return model

def conv(num_kinds, input_length):
    embd = 64
    ksize = 3
    filters=64
    dp = 0.1
    model = Sequential()
    model.add(Embedding(num_kinds, embd, input_length=length))
    model.add(Dropout(dp))
    model.add(Conv1D(filters, ksize, activation='relu'))
    model.add(Dropout(dp))
    model.add(Conv1D(filters, ksize, activation='relu'))
    model.add(Dropout(dp))
    model.add(MaxPooling1D(3))
    model.add(Dropout(dp))
    model.add(Flatten())
    model.add(Dropout(dp))
    model.add(Dense(num_kinds, activation='softmax'))
    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    model.summary()
    return model
