// Copyright 2017 Ivan Yakimov
// Licensed under the Apache License, Version 2.0
// e-mail: ivan.yakimov.research@yandex.ru

#ifndef __FUNC_CHECKER__
#define __FUNC_CHECKER__

#include <memory>
#include <iostream>
#include <fstream>
#include <string>
#include <unordered_set>
#include <cassert>

#include "llvm/IR/Instruction.h"
#include "llvm/IR/Function.h"

namespace trans {
  class FuncChecker;
  using FuncCheckerPtr = std::shared_ptr<FuncChecker>;
  /// Stores names of functions from the target library (i.e. Open GL)
  using FuncSet = std::unordered_set<std::string>;
  /// Checks if the function influences the transformation process (needs to be count or traced)
  class FuncChecker {
  public:
    static FuncCheckerPtr GetInstance();
    bool Check(llvm::Function* func);
    FuncChecker();
    ~FuncChecker();
  private:
    FuncSet func_set_;
    static FuncCheckerPtr instance_;
  };
};

#endif
