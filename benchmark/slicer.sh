#!/bin/bash

OPTIND=1
FILE=$1
NAME=$(basename $FILE .trace)
echo "Slicing file '$FILE'"
if [[ ! -f $FILE ]]; then
    echo "Error: $FILE not a file">&2 && exit 1; fi

# Split trace $FILE into slices
csplit --elide-empty-files --suppress-matched --quiet --digits=2 --prefix=$NAME --suffix-format='%02d.slice' $FILE "/^#$/" "{*}"
