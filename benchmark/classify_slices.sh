#!/bin/bash

if [[ ! -d meta_slices ]]; then
    mkdir meta_slices; fi

for s in slices/*.slice; do
    echo "Processing $s"
    for f in $(cat $s);do
	basename $(egrep -wlir $f ../classes/separated/*.class) '.class' 
    done > "meta_slices/$(basename $s)"
done

touch all-meta-sliced
