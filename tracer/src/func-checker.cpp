#include "func-checker.hpp"

namespace trans {
  FuncCheckerPtr FuncChecker::instance_ = nullptr;

  FuncChecker::FuncChecker() {
    // open a file
    const char* the_list="uniq-called-gl.list";
    std::ifstream infile(the_list);
    if (infile.fail()) {
      std::cerr << "Can't open file" << the_list << std::endl;
      infile.clear();
      exit(1);
    }
    // process the file
    std::string func;
    while (infile >> func) {
      // add a new item to the set
      assert (func_set_.find(func) == func_set_.end());
      func_set_.insert(func);
    }
    
    assert (infile.eof()); // EOF reached
    infile.close(); // close the file
  }
  
  FuncChecker::~FuncChecker() {
  }

  bool FuncChecker::Check(llvm::Function* func) {
    auto ty = func->getReturnType();
    auto name = func->getName().str();
    if (not ty->isVoidTy()) {
      return false; // TODO: Cannot remove void functions
    }
    if (func_set_.find(name) != func_set_.end()) {
      return true;
      //TODO: replace 'magical constant string'
    } else if (name == "__skipped_handler__") {
      assert (false and "not implemented");
      return false;
    } else {
      return false; }
  }
  
  FuncCheckerPtr FuncChecker::GetInstance() {
    if (instance_ == nullptr) {
      // create new instance
      instance_ = std::make_shared<FuncChecker>();
      return instance_;
    } else {
      return instance_;
    }
  }
  
};
