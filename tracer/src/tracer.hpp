#ifndef __TRACER_PASS__
#define __TRACER_PASS__

/// llvm
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/IR/Module.h"
#include "llvm/Transforms/Utils/Cloning.h"

/// the project
#include "call-tracer.hpp"
#include "context-maker.hpp"

#include <system_error>
#include <string>

namespace {
  struct TracerPass : public llvm::ModulePass {
    static char ID;
    TracerPass() : ModulePass(ID){}
    bool runOnModule(llvm::Module &module) override;
  };
};

char TracerPass::ID = 0;
static llvm::RegisterPass<TracerPass> X("ll-tracer", "static llvm code tracer", false, false);

#endif
