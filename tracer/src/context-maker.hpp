// Copyright 2017 Ivan Yakimov
// Licensed under the Apache License, Version 2.0
// e-mail: ivan.yakimov.research@yandex.ru
#ifndef __CONTEXT_MAKER__
#define __CONTEXT_MAKER__

#include "llvm/IR/InstVisitor.h"
#include "llvm/Support/raw_ostream.h"

#include "func-checker.hpp"

namespace context {
  class ContextVisitor : public llvm::InstVisitor<ContextVisitor> {
  public:
    void visitCallInst(llvm::CallInst &call);
    ContextVisitor();
    ~ContextVisitor();
  private:
  };
}

#endif
