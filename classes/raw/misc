/*
 * Miscellaneous
 */

GLAPI void GLAPIENTRY glClearIndex( GLfloat c );
GLAPI void GLAPIENTRY glIndexMask( GLuint mask );
GLAPI void GLAPIENTRY glColorMask( GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha );
GLAPI void GLAPIENTRY glAlphaFunc( GLenum func, GLclampf ref );
GLAPI void GLAPIENTRY glBlendFunc( GLenum sfactor, GLenum dfactor );
GLAPI void GLAPIENTRY glLogicOp( GLenum opcode );
GLAPI void GLAPIENTRY glCullFace( GLenum mode );
GLAPI void GLAPIENTRY glFrontFace( GLenum mode );
GLAPI void GLAPIENTRY glPointSize( GLfloat size );
GLAPI void GLAPIENTRY glLineWidth( GLfloat width );
GLAPI void GLAPIENTRY glLineStipple( GLint factor, GLushort pattern );
GLAPI void GLAPIENTRY glPolygonMode( GLenum face, GLenum mode );
GLAPI void GLAPIENTRY glPolygonOffset( GLfloat factor, GLfloat units );
GLAPI void GLAPIENTRY glPolygonStipple( const GLubyte *mask );
GLAPI void GLAPIENTRY glGetPolygonStipple( GLubyte *mask );
GLAPI void GLAPIENTRY glEdgeFlag( GLboolean flag );
GLAPI void GLAPIENTRY glEdgeFlagv( const GLboolean *flag );
GLAPI void GLAPIENTRY glScissor( GLint x, GLint y, GLsizei width, GLsizei height);
GLAPI void GLAPIENTRY glClipPlane( GLenum plane, const GLdouble *equation );
GLAPI void GLAPIENTRY glGetClipPlane( GLenum plane, GLdouble *equation );
GLAPI void GLAPIENTRY glDrawBuffer( GLenum mode );
GLAPI void GLAPIENTRY glReadBuffer( GLenum mode );
GLAPI GLboolean GLAPIENTRY glIsEnabled( GLenum cap );
GLAPI void GLAPIENTRY glEnableClientState( GLenum cap );  /* 1.1 */
GLAPI void GLAPIENTRY glDisableClientState( GLenum cap );  /* 1.1 */
GLAPI void GLAPIENTRY glGetBooleanv( GLenum pname, GLboolean *params );
GLAPI void GLAPIENTRY glGetDoublev( GLenum pname, GLdouble *params );
GLAPI void GLAPIENTRY glGetFloatv( GLenum pname, GLfloat *params );
GLAPI void GLAPIENTRY glGetIntegerv( GLenum pname, GLint *params );
GLAPI void GLAPIENTRY glPushAttrib( GLbitfield mask );
GLAPI void GLAPIENTRY glPopAttrib( void );
GLAPI void GLAPIENTRY glPushClientAttrib( GLbitfield mask );  /* 1.1 */
GLAPI void GLAPIENTRY glPopClientAttrib( void );  /* 1.1 */
GLAPI GLint GLAPIENTRY glRenderMode( GLenum mode );
GLAPI GLenum GLAPIENTRY glGetError( void );
GLAPI const GLubyte * GLAPIENTRY glGetString( GLenum name );
GLAPI void GLAPIENTRY glFinish( void );
GLAPI void GLAPIENTRY glHint( GLenum target, GLenum mode );

/*
 * Fog
 */

GLAPI void GLAPIENTRY glFogf( GLenum pname, GLfloat param );
GLAPI void GLAPIENTRY glFogi( GLenum pname, GLint param );
GLAPI void GLAPIENTRY glFogfv( GLenum pname, const GLfloat *params );
GLAPI void GLAPIENTRY glFogiv( GLenum pname, const GLint *params );


/* 1.1 functions */

GLAPI void GLAPIENTRY glGenTextures( GLsizei n, GLuint *textures );
GLAPI void GLAPIENTRY glDeleteTextures( GLsizei n, const GLuint *textures);
GLAPI void GLAPIENTRY glBindTexture( GLenum target, GLuint texture );
GLAPI void GLAPIENTRY glPrioritizeTextures( GLsizei n,
                                            const GLuint *textures,
                                            const GLclampf *priorities );
GLAPI GLboolean GLAPIENTRY glAreTexturesResident( GLsizei n,
                                                  const GLuint *textures,
                                                  GLboolean *residences );
GLAPI GLboolean GLAPIENTRY glIsTexture( GLuint texture );
GLAPI void GLAPIENTRY glTexSubImage1D( GLenum target, GLint level,
                                       GLint xoffset,
                                       GLsizei width, GLenum format,
                                       GLenum type, const GLvoid *pixels );
GLAPI void GLAPIENTRY glTexSubImage2D( GLenum target, GLint level,
                                       GLint xoffset, GLint yoffset,
                                       GLsizei width, GLsizei height,
                                       GLenum format, GLenum type,
                                       const GLvoid *pixels );
GLAPI void GLAPIENTRY glCopyTexImage1D( GLenum target, GLint level,
                                        GLenum internalformat,
                                        GLint x, GLint y,
                                        GLsizei width, GLint border );
GLAPI void GLAPIENTRY glCopyTexImage2D( GLenum target, GLint level,
                                        GLenum internalformat,
                                        GLint x, GLint y,
                                        GLsizei width, GLsizei height,
                                        GLint border );
GLAPI void GLAPIENTRY glCopyTexSubImage1D( GLenum target, GLint level,
                                           GLint xoffset, GLint x, GLint y,
                                           GLsizei width );
GLAPI void GLAPIENTRY glCopyTexSubImage2D( GLenum target, GLint level,
                                           GLint xoffset, GLint yoffset,
                                           GLint x, GLint y,
                                           GLsizei width, GLsizei height );



/*
 * OpenGL 1.3
 */

/* multitexture */

GLAPI void GLAPIENTRY glActiveTexture( GLenum texture );
GLAPI void GLAPIENTRY glClientActiveTexture( GLenum texture );
GLAPI void GLAPIENTRY glCompressedTexImage1D( GLenum target, GLint level, GLenum internalformat, GLsizei width, GLint border, GLsizei imageSize, const GLvoid *data );
GLAPI void GLAPIENTRY glCompressedTexImage2D( GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const GLvoid *data );
GLAPI void GLAPIENTRY glCompressedTexImage3D( GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const GLvoid *data );
GLAPI void GLAPIENTRY glCompressedTexSubImage1D( GLenum target, GLint level, GLint xoffset, GLsizei width, GLenum format, GLsizei imageSize, const GLvoid *data );
GLAPI void GLAPIENTRY glCompressedTexSubImage2D( GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const GLvoid *data );
GLAPI void GLAPIENTRY glCompressedTexSubImage3D( GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const GLvoid *data );
GLAPI void GLAPIENTRY glGetCompressedTexImage( GLenum target, GLint lod, GLvoid *img );
GLAPI void GLAPIENTRY glMultiTexCoord1d( GLenum target, GLdouble s );
GLAPI void GLAPIENTRY glMultiTexCoord1dv( GLenum target, const GLdouble *v );
GLAPI void GLAPIENTRY glMultiTexCoord1f( GLenum target, GLfloat s );
GLAPI void GLAPIENTRY glMultiTexCoord1fv( GLenum target, const GLfloat *v );
GLAPI void GLAPIENTRY glMultiTexCoord1i( GLenum target, GLint s );
GLAPI void GLAPIENTRY glMultiTexCoord1iv( GLenum target, const GLint *v );
GLAPI void GLAPIENTRY glMultiTexCoord1s( GLenum target, GLshort s );
GLAPI void GLAPIENTRY glMultiTexCoord1sv( GLenum target, const GLshort *v );
GLAPI void GLAPIENTRY glMultiTexCoord2d( GLenum target, GLdouble s, GLdouble t );
GLAPI void GLAPIENTRY glMultiTexCoord2dv( GLenum target, const GLdouble *v );
GLAPI void GLAPIENTRY glMultiTexCoord2f( GLenum target, GLfloat s, GLfloat t );
GLAPI void GLAPIENTRY glMultiTexCoord2fv( GLenum target, const GLfloat *v );
GLAPI void GLAPIENTRY glMultiTexCoord2i( GLenum target, GLint s, GLint t );
GLAPI void GLAPIENTRY glMultiTexCoord2iv( GLenum target, const GLint *v );
GLAPI void GLAPIENTRY glMultiTexCoord2s( GLenum target, GLshort s, GLshort t );
GLAPI void GLAPIENTRY glMultiTexCoord2sv( GLenum target, const GLshort *v );
GLAPI void GLAPIENTRY glMultiTexCoord3d( GLenum target, GLdouble s, GLdouble t, GLdouble r );
GLAPI void GLAPIENTRY glMultiTexCoord3dv( GLenum target, const GLdouble *v );
GLAPI void GLAPIENTRY glMultiTexCoord3f( GLenum target, GLfloat s, GLfloat t, GLfloat r );
GLAPI void GLAPIENTRY glMultiTexCoord3fv( GLenum target, const GLfloat *v );
GLAPI void GLAPIENTRY glMultiTexCoord3i( GLenum target, GLint s, GLint t, GLint r );
GLAPI void GLAPIENTRY glMultiTexCoord3iv( GLenum target, const GLint *v );
GLAPI void GLAPIENTRY glMultiTexCoord3s( GLenum target, GLshort s, GLshort t, GLshort r );
GLAPI void GLAPIENTRY glMultiTexCoord3sv( GLenum target, const GLshort *v );
GLAPI void GLAPIENTRY glMultiTexCoord4d( GLenum target, GLdouble s, GLdouble t, GLdouble r, GLdouble q );
GLAPI void GLAPIENTRY glMultiTexCoord4dv( GLenum target, const GLdouble *v );
GLAPI void GLAPIENTRY glMultiTexCoord4f( GLenum target, GLfloat s, GLfloat t, GLfloat r, GLfloat q );
GLAPI void GLAPIENTRY glMultiTexCoord4fv( GLenum target, const GLfloat *v );
GLAPI void GLAPIENTRY glMultiTexCoord4i( GLenum target, GLint s, GLint t, GLint r, GLint q );
GLAPI void GLAPIENTRY glMultiTexCoord4iv( GLenum target, const GLint *v );
GLAPI void GLAPIENTRY glMultiTexCoord4s( GLenum target, GLshort s, GLshort t, GLshort r, GLshort q );
GLAPI void GLAPIENTRY glMultiTexCoord4sv( GLenum target, const GLshort *v );
GLAPI void GLAPIENTRY glLoadTransposeMatrixd( const GLdouble m[16] );
GLAPI void GLAPIENTRY glLoadTransposeMatrixf( const GLfloat m[16] );
GLAPI void GLAPIENTRY glMultTransposeMatrixd( const GLdouble m[16] );
GLAPI void GLAPIENTRY glMultTransposeMatrixf( const GLfloat m[16] );
GLAPI void GLAPIENTRY glSampleCoverage( GLclampf value, GLboolean invert );
typedef void (APIENTRYP PFNGLACTIVETEXTUREPROC) (GLenum texture);
typedef void (APIENTRYP PFNGLSAMPLECOVERAGEPROC) (GLclampf value, GLboolean invert);
typedef void (APIENTRYP PFNGLCOMPRESSEDTEXIMAGE3DPROC) (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const GLvoid *data);
typedef void (APIENTRYP PFNGLCOMPRESSEDTEXIMAGE2DPROC) (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const GLvoid *data);
typedef void (APIENTRYP PFNGLCOMPRESSEDTEXIMAGE1DPROC) (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLint border, GLsizei imageSize, const GLvoid *data);
typedef void (APIENTRYP PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC) (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const GLvoid *data);
typedef void (APIENTRYP PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC) (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const GLvoid *data);
typedef void (APIENTRYP PFNGLCOMPRESSEDTEXSUBIMAGE1DPROC) (GLenum target, GLint level, GLint xoffset, GLsizei width, GLenum format, GLsizei imageSize, const GLvoid *data);
typedef void (APIENTRYP PFNGLGETCOMPRESSEDTEXIMAGEPROC) (GLenum target, GLint level, GLvoid *img);

/*
 * OpenGL 1.2
 */

GLAPI void GLAPIENTRY glDrawRangeElements( GLenum mode, GLuint start,
	GLuint end, GLsizei count, GLenum type, const GLvoid *indices );
GLAPI void GLAPIENTRY glTexImage3D( GLenum target, GLint level,
                                      GLint internalFormat,
                                      GLsizei width, GLsizei height,
                                      GLsizei depth, GLint border,
                                      GLenum format, GLenum type,
                                      const GLvoid *pixels );
GLAPI void GLAPIENTRY glTexSubImage3D( GLenum target, GLint level,
                                         GLint xoffset, GLint yoffset,
                                         GLint zoffset, GLsizei width,
                                         GLsizei height, GLsizei depth,
                                         GLenum format,
                                         GLenum type, const GLvoid *pixels);
GLAPI void GLAPIENTRY glCopyTexSubImage3D( GLenum target, GLint level,
                                             GLint xoffset, GLint yoffset,
                                             GLint zoffset, GLint x,
                                             GLint y, GLsizei width,
                                             GLsizei height );
typedef void (APIENTRYP PFNGLDRAWRANGEELEMENTSPROC) (GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const GLvoid *indices);
typedef void (APIENTRYP PFNGLTEXIMAGE3DPROC) (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const GLvoid *pixels);
typedef void (APIENTRYP PFNGLTEXSUBIMAGE3DPROC) (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const GLvoid *pixels);
typedef void (APIENTRYP PFNGLCOPYTEXSUBIMAGE3DPROC) (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height);

/*
 * Vertex Arrays  (1.1)
 */

GLAPI void GLAPIENTRY glVertexPointer( GLint size, GLenum type,
                                       GLsizei stride, const GLvoid *ptr );
GLAPI void GLAPIENTRY glNormalPointer( GLenum type, GLsizei stride,
                                       const GLvoid *ptr );
GLAPI void GLAPIENTRY glColorPointer( GLint size, GLenum type,
                                      GLsizei stride, const GLvoid *ptr );
GLAPI void GLAPIENTRY glIndexPointer( GLenum type, GLsizei stride,
                                      const GLvoid *ptr );
GLAPI void GLAPIENTRY glTexCoordPointer( GLint size, GLenum type,
                                         GLsizei stride, const GLvoid *ptr );
GLAPI void GLAPIENTRY glEdgeFlagPointer( GLsizei stride, const GLvoid *ptr );
GLAPI void GLAPIENTRY glGetPointerv( GLenum pname, GLvoid **params );
GLAPI void GLAPIENTRY glArrayElement( GLint i );
GLAPI void GLAPIENTRY glDrawArrays( GLenum mode, GLint first, GLsizei count );
GLAPI void GLAPIENTRY glDrawElements( GLenum mode, GLsizei count,
                                      GLenum type, const GLvoid *indices );
GLAPI void GLAPIENTRY glInterleavedArrays( GLenum format, GLsizei stride,
                                           const GLvoid *pointer );

/*
 * GL_ARB_imaging
 */

GLAPI void GLAPIENTRY glColorTable( GLenum target, GLenum internalformat,
                                    GLsizei width, GLenum format,
                                    GLenum type, const GLvoid *table );
GLAPI void GLAPIENTRY glColorSubTable( GLenum target,
                                       GLsizei start, GLsizei count,
                                       GLenum format, GLenum type,
                                       const GLvoid *data );
GLAPI void GLAPIENTRY glColorTableParameteriv(GLenum target, GLenum pname,
                                              const GLint *params);
GLAPI void GLAPIENTRY glColorTableParameterfv(GLenum target, GLenum pname,
                                              const GLfloat *params);
GLAPI void GLAPIENTRY glCopyColorSubTable( GLenum target, GLsizei start,
                                           GLint x, GLint y, GLsizei width );
GLAPI void GLAPIENTRY glCopyColorTable( GLenum target, GLenum internalformat,
                                        GLint x, GLint y, GLsizei width );
GLAPI void GLAPIENTRY glGetColorTable( GLenum target, GLenum format,
                                       GLenum type, GLvoid *table );
GLAPI void GLAPIENTRY glGetColorTableParameterfv( GLenum target, GLenum pname,
                                                  GLfloat *params );
GLAPI void GLAPIENTRY glGetColorTableParameteriv( GLenum target, GLenum pname,
                                                  GLint *params );
GLAPI void GLAPIENTRY glBlendEquation( GLenum mode );
GLAPI void GLAPIENTRY glBlendColor( GLclampf red, GLclampf green,
                                    GLclampf blue, GLclampf alpha );
GLAPI void GLAPIENTRY glHistogram( GLenum target, GLsizei width,
				   GLenum internalformat, GLboolean sink );
GLAPI void GLAPIENTRY glResetHistogram( GLenum target );
GLAPI void GLAPIENTRY glGetHistogram( GLenum target, GLboolean reset,
				      GLenum format, GLenum type,
				      GLvoid *values );
GLAPI void GLAPIENTRY glGetHistogramParameterfv( GLenum target, GLenum pname,
						 GLfloat *params );
GLAPI void GLAPIENTRY glGetHistogramParameteriv( GLenum target, GLenum pname,
						 GLint *params );
GLAPI void GLAPIENTRY glMinmax( GLenum target, GLenum internalformat,
				GLboolean sink );
GLAPI void GLAPIENTRY glResetMinmax( GLenum target );
GLAPI void GLAPIENTRY glGetMinmax( GLenum target, GLboolean reset,
                                   GLenum format, GLenum types,
                                   GLvoid *values );
GLAPI void GLAPIENTRY glGetMinmaxParameterfv( GLenum target, GLenum pname,
					      GLfloat *params );
GLAPI void GLAPIENTRY glGetMinmaxParameteriv( GLenum target, GLenum pname,
					      GLint *params );
GLAPI void GLAPIENTRY glConvolutionFilter1D( GLenum target,
	GLenum internalformat, GLsizei width, GLenum format, GLenum type,
	const GLvoid *image );
GLAPI void GLAPIENTRY glConvolutionFilter2D( GLenum target,
	GLenum internalformat, GLsizei width, GLsizei height, GLenum format,
	GLenum type, const GLvoid *image );
GLAPI void GLAPIENTRY glConvolutionParameterf( GLenum target, GLenum pname,
	GLfloat params );
GLAPI void GLAPIENTRY glConvolutionParameterfv( GLenum target, GLenum pname,
	const GLfloat *params );
GLAPI void GLAPIENTRY glConvolutionParameteri( GLenum target, GLenum pname,
	GLint params );
GLAPI void GLAPIENTRY glConvolutionParameteriv( GLenum target, GLenum pname,
	const GLint *params );
GLAPI void GLAPIENTRY glCopyConvolutionFilter1D( GLenum target,
	GLenum internalformat, GLint x, GLint y, GLsizei width );
GLAPI void GLAPIENTRY glCopyConvolutionFilter2D( GLenum target,
	GLenum internalformat, GLint x, GLint y, GLsizei width,
	GLsizei height);
GLAPI void GLAPIENTRY glGetConvolutionFilter( GLenum target, GLenum format,
	GLenum type, GLvoid *image );
GLAPI void GLAPIENTRY glGetConvolutionParameterfv( GLenum target, GLenum pname,
	GLfloat *params );
GLAPI void GLAPIENTRY glGetConvolutionParameteriv( GLenum target, GLenum pname,
	GLint *params );
GLAPI void GLAPIENTRY glSeparableFilter2D( GLenum target,
	GLenum internalformat, GLsizei width, GLsizei height, GLenum format,
	GLenum type, const GLvoid *row, const GLvoid *column );
GLAPI void GLAPIENTRY glGetSeparableFilter( GLenum target, GLenum format,
	GLenum type, GLvoid *row, GLvoid *column, GLvoid *span );

// Added manually!!!
glAreTexturesResidentEXT()
glBlendEquationEXT()
glCopyTexSubImage2DEXT()
glDeleteTexturesEXT()
glEdgeFlagPointerEXT()
glFogCoordfEXT()
glGetHistogramEXT()
glGetPointervEXT()
glGetTexImage()
glHistogramEXT()
glIsTextureEXT()
glMultiDrawElementsEXT()
glNormalPointerEXT()
glPointParameterfEXT()
glPointParameterfvEXT()
glPolygonOffsetEXT()
glPrioritizeTexturesEXT()
glTexCoordPointerEXT()
glutSetupVideoResizing()
glutVideoResizeGet()
glWindowPos2fMESA()

