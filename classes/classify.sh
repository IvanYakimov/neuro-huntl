#!/bin/bash

####################################################################################################
# For given lists of headers and list of called GL functions creates files containing classes
####################################################################################################

get_classes() {
    f=$1
    most_called=$2
    grep -horf $most_called $f 
}

remove_braces() {
    while read line; do
	echo $line | sed 's/.$//'
    done
}

target_name() {
    f=$1
    echo "separated/$(basename $f).class"
}

if [[ ! -d 'separated' ]]; then
    mkdir 'separated'
fi

# Is tmp really needed?
tmp=$(mktemp)
awk '{print $0"("}' uniq-called-gl.list > $tmp

for f in raw/*; do
    tmpclass=$(mktemp)
    get_classes $f $tmp | remove_braces > $tmpclass
    grep -f $tmpclass -w distribution-gl.list | awk {'print $2 " " $1'} | sort -k1 > $(target_name $f)
    rm $tmpclass
done

rm $tmp

################################################################################
# Produces distribution of classes.

for list in separated/*; do
    echo "$(basename $list .class) $(cat $list |wc -l) $(cat $list | awk '{print $2}' | paste -sd+ | bc)"
done | sort -k3 -n -r > distribution-classes.list

############################################################
# Print classes
cat distribution-classes.list | awk {'print $1'} > classes.list
