#!/usr/bin/python2.7

################################################################################
# Import
import json
import argparse

################################################################################
#

parser = argparse.ArgumentParser(description='arrange groupped slices into data batches of equal size')
parser.add_argument("-n", help="number of groups of batches in the dataset", type=int)
parser.add_argument("-e", help="precision", type=float)
parser.add_argument("-i", help="input file", type=str)
parser.add_argument("-o", help="output file", type=str)
args = parser.parse_args()
target = args.i
dest = args.o
epsilon = args.e
num_groups = args.n

if not (target and dest and epsilon and num_groups):
    print "Error on parsing string, use -h"
    exit(1)

data={}
arranged=[{} for _ in range(num_groups)]
weights=[0]*num_groups

################################################################################
# Functions
def getWeight(m):
    return sum(m.values())

def printData(data):
    for d in data:
        print d, getWeight(data[d]), data[d]

def arrange(data, arranged, weights):
    for aslice in data:
        # get weight for the next slice in the data map
        weight = getWeight(data[aslice])
        # get index of the map with minimal weight in the arranged map list
        idx = weights.index(min(weights))
        # add the map into the appropriate place within the arranged map list
        arranged[idx][aslice] = data[aslice]
        # updata weights
        weights[idx] += weight
    return arranged, weights

def load(target):
    with open(target) as f:
        line = f.read()
    return json.loads(line)

def check(weights, epsilon):
    w = sum(weights)
    ideal = 1/float(num_groups)
    err = [(lambda x: abs(float(x)/float(w)-ideal))(x) for x in weights]
    accuracy = sum(err)/len(err)
    print "acc: {0}".format(accuracy)
    if max(err) >= epsilon:
        print "Bad accuracy"
        exit(1)

def save(dest, data):
    # Save
    with open(dest, 'w') as f:
        f.write(json.dumps(data))

        
################################################################################
# Main body
data = load(target)
arranged, weights = arrange(data, arranged, weights)
check(weights, epsilon)
save(dest, arranged)
