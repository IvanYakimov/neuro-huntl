#!/bin/bash

############################################################
ABOUT="
ABOUT: \n
Performs transformation of the target program. \n
User should provide the input file as a command line argument. \n
The format is: $0 -i <input file>. \n
Note that the input file should be written in C languange. \n
On the other hand, the result of the transformation process is a collection of files. \n
Each file has a name <original>.bc<version>.ll, where <original> is a name of the original C file 
and <version> is a number version. \n
Each version contains single skipped (void) GL-function call (except <original>.bc.ll - it contains no skipped calls).
"
############################################################

set -e

# Settings
LLVM_FLAGS='-emit-llvm -O0'
CLANG=clang-3.5

# Command line arguments parsing: http://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
OPTINT=1

FILE=$1
if [[ ! -f $FILE ]]; then
    echo "Not a file: '$FILE'">&2 && exit 1; fi

NAME=$(basename $FILE .c)

$CLANG -c $FILE -o $NAME.bc $LLVM_FLAGS
opt -load=./tracer.so -ll-tracer $NAME.bc
rm $NAME.bc
