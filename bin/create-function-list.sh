#!/bin/bash

############################################################
# All functions
# Get total amount of called functions
for f in sources/*.c; do
    cscope ${f} -L -2 '.*' | awk -F ' ' '{print $2}'
done > 'all-called.list'
# Get uniq function names used in the examples
cat 'all-called.list' | sort | uniq > 'uniq-called.list'

############################################################
# GL functions
# Copied from: https://stackoverflow.com/questions/1570917/extracting-c-c-function-prototypes
# Extract function declarations from the GL include files
ctags-exuberant -x --c-kinds=p /usr/include/GL/*.h | grep -Eo '^[^ ]+' | sort | uniq > 'gl-decl.list';
# Create 'uniq' list of names of GL functions called in the examples.
comm -12 'gl-decl.list' 'uniq-called.list' > 'uniq-called-gl.list';
# Count exact number of calls of GL functions in the examples.
grep -wf 'uniq-called-gl.list' 'all-called.list' | sort | uniq -c | sort -nrk 1 | sed 's/^ *//;s/ *$//' > distribution-gl.list

