#!/usr/bin/python2.7

import json
import fileinput

curname=""
curlist={}
slicemap = {}

for line in fileinput.input():
 str = line.rstrip()
 if str[0] == ">":
  name=str[1:]
  slicemap[name] = curlist
  curlist = {}
 else:
  splitted = str.split()
  key = splitted[0]
  val = int(splitted[1])
  curlist[key] = val

print json.dumps(slicemap)
